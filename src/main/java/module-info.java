module rchart {
	exports de.grogra.rchart;

	requires platform;
	requires platform.core;
	requires platform.swing;
	requires imp;
	requires utilities;
	requires java.datatransfer;
	requires java.desktop;
}